package aytocalc;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class EvaluaTest {

	@Test
	void testEvaluaOperacion() {
		Evalua calculadora = new Evalua();
		double resultado = calculadora.calcula (10.0,10.0);
		assertEquals (0.0, resultado);
		
		resultado = calculadora.calcula (10.0,11.0);
		assertEquals (1.0, resultado);		
		
		resultado = calculadora.calcula (11.0,10.0);
		assertEquals (1.0, resultado);		

	}

}
