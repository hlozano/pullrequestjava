package aytocalc;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class RestaTest {

	@Test
	void testRestaDosNumerosNegativos() {
		Resta resta = new Resta();
		int rdo = resta.calcula(-4, -9);
		System.out.println("restaNumNega= " + rdo);
		assertEquals(5,0,rdo);
	}


}
